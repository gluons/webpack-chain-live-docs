window.$docsify = {
	basePath:
		'https://raw.githubusercontent.com/neutrinojs/webpack-chain/master/',
	repo: 'neutrinojs/webpack-chain',
	name: 'webpack-chain',
	plugins: [
		EditOnGithubPlugin.create(
			'https://github.com/neutrinojs/webpack-chain/blob/master/'
		)
	]
};
